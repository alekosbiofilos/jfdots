alias vim="nvim"
alias bat="bat --style=grid"
# activate Rust
alias activate_rust="source /home/alekos/.cargo/env"
# alternative to ls
alias ls="exa --icons --colour=always"
alias lst="exa --icons --colour=always -T | batcat --style 'grid'"
# Change governor
alias django="python manage.py"
alias conda_act="eval $(/home/alekos/miniconda3/bin/conda shell.zsh hook)"
alias mani_aws="export AWS_DEFAULT_PROFILE=mani"
