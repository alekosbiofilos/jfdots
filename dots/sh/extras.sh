export PATH="/home/alekos/.gem/ruby/2.7.0/bin":$PATH
export PATH="/home/alekos/apps/bin":$PATH
export PATH="/home/alekos/.gem/ruby/2.6.0/bin":$PATH
export PATH="/var/lib/flatpak/exports/bin:$PATH"
export PATH="/home/alekos/.cargo/bin":$PATH
export XDG_DATA_DIRS="~/.local/share/flatpak/exports/share/applications:/var/lib/flatpak/exports/share/applications:$XDG_DATA_DIRS"
export PATH="/home/alekos/.local/bin":$PATH
export EDITOR="nvim"
export TERM="xterm-256color"
# For FuzzyFinder
source /home/alekos/dots/fzf/completion.sh
source /home/alekos/dots/fzf/key-bindings.sh
export FZF_DEFAULT_COMMAND='fd-find --type f'
export FZF_CTRL_T_COMMAND=$FZF_DEFAULT_COMMAND
export FZF_DEFAULT_OPTS='--preview "[[ $(file --mime {}) =~ binary ]] && echo {} is a binary file || (bat --style=numbers --color=always {} || highlight -O ansi -l {} || coderay {} || rougify {} || bat {}) 2> /dev/null | head -500"'
# Colorize man pages with bat
export MANROFFOPT="-c"
export MANPAGER="sh -c 'col -bx | bat -l man -p'"
#Cuda
export PATH=/opt/cuda/bin:$PATH
export LD_LIBRARY_PATH=/opt/cuda/lib64:/opt/cuda/lib:$HOME/lib:$LD_LIBRARY_PATH
export C_INCLUDE_PATH=/opt/cuda/include:$C_INCLUDE_PATH
export CPLUS_INCLUDE_PATH=/opt/cuda/include:$CPLUS_INCLUDE_PATH
export HOST_COMPILER=cuda-g++
# More things for aws
export NVM_DIR="$HOME/.nvm"
export PATH=$HOME/bin:$PATH
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm
#
#PATH=$HOME/bin:$PATH
export STARSHIP_CONFIG=~/dots/starship.toml
# For VScode
[[ "$TERM_PROGRAM" == "vscode" ]] && . "$(code --locate-shell-integration-path bash)"
