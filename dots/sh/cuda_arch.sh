# Paths for cuda
export HOST_COMPILER=cuda-g++

PATH=$PATH:$HOME/.local/bin:$HOME/bin:/usr/local/cuda-11.1/bin
export PATH=/usr/local/cuda-11.1/bin${PATH:+:${PATH}}
export LD_LIBRARY_PATH=/usr/local/cuda-11.1/lib64:${LD_LIBRARY_PATH:+:${LD_LIBRARY_PATH}}
LIBRARY_PATH=$LIBRARY_PATH:$HOME/lib
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/cuda-11.1/lib64:$HOME/lib
CPLUS_INCLUDE_PATH=$CPLUS_INCLUDE_PATH:$HOME/NVIDIA_CUDA-11.1_Samples/common/inc:$HOME/include
 
export PATH
export LIBRARY_PATH
export LD_LIBRARY_PATH

