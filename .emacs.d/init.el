(setq inhibit-startup-screen t)
(scroll-bar-mode -1)
(tool-bar-mode -1)
(tooltip-mode -1)
(menu-bar-mode -1)
(set-fringe-mode 10)
(setq visible-bell t)

(set-face-attribute 'default nil :font "JetBrainsMono Nerd Font Mono" :height 150)
(set-face-attribute 'fixed-pitch nil :font "JetBrainsMono Nerd Font Mono" :height 150)
(set-face-attribute 'variable-pitch nil :font "Roboto" :height 150)

(require 'package)
(setq package-archives '(("melpa" . "https://melpa.org/packages/")
                         ("org" . "https://orgmode.org/elpa/")
                         ("elpa" . "https://elpa.gnu.org/packages/")))

(package-initialize)
(unless package-archive-contents
 (package-refresh-contents))

;; Initialize use-package on non-Linux platforms
(unless (package-installed-p 'use-package)
   (package-install 'use-package))

(require 'use-package)
(setq use-package-always-ensure t)

(use-package doom-themes
  :ensure t
  :config
  (setq soom-themes-enable-bold t
	doom-themes-enable-italics t)
  (load-theme 'doom-tomorrow-night t)
  ;;(load-theme 'doom-Iosvkem t)
  (doom-themes-visual-bell-config)
  ;; Enable custom neotree theme (all-the-icons must be installed!)
  ;;(doom-themes-neotree-config)
  ;; or for treemacs users
  (setq doom-themes-treemacs-theme "doom-material-night") ; use "doom-colors" for less minimal icon theme
  (doom-themes-treemacs-config)
  ;; Corrects (and improves) org-mode's native fontification.
  (doom-themes-org-config))

(use-package all-the-icons)

(use-package doom-modeline
  :ensure t
  :init (doom-modeline-mode 1)
  :custom ((doom-modeline-height 5)))
;; doom-mode config
;; How tall the mode-line should be. It's only respected in GUI.
;; If the actual char height is larger, it respects the actual height.
(setq doom-modeline-height 25)

;; How wide the mode-line bar should be. It's only respected in GUI.
(setq doom-modeline-bar-width 4)
;; environments
(setq doom-modeline-env-version t)
(setq doom-modeline-env-enable-python  t)
(setq doom-modeline-before-update-env-hook nil)
(setq doom-modeline-after-update-env-hook nil)
(setq doom-modeline-lsp t)
(use-package ghub)
;; Whether display the GitHub notifications. It requires `ghub' package.
(setq doom-modeline-github nil)
(setq doom-modeline-continuous-word-count-modes '(markdown-mode gfm-mode org-mode))
(setq doom-modeline-icon (display-graphic-p))
;; Whether display the icon for `major-mode'. It respects `doom-modeline-icon'.
(setq doom-modeline-major-mode-icon t)
;; Whether display the colorful icon for `major-mode'.
;; It respects `all-the-icons-color-icons'.
(setq doom-modeline-major-mode-color-icon t)
;; Whether display the icon for the buffer state. It respects `doom-modeline-icon'.
(setq doom-modeline-buffer-state-icon t)
;; Whether display the modification icon for the buffer.
;; It respects `doom-modeline-icon' and `doom-modeline-buffer-state-icon'.
(setq doom-modeline-buffer-modification-icon t)
;; how often to check github
(setq doom-modeline-github-interval (* 30 60))

;; line numbers

(column-number-mode)
(global-display-line-numbers-mode t)


;; Disable line numbers for some modes
;; here, I am adding the display-line-numbers-mode 0 to each
;; of the modes in the list
(dolist (mode '(org-mode-hook
		shell-mode-hook
		term-mode-hook
		eshell-mode-hook
          vterm-mode-hook
		markdown-mode-hook
		gfm-mode-hook
		treemacs-mode-hook))
  (add-hook mode (lambda () (display-line-numbers-mode 0))))

;; rainbow delimiters for parentheses
(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

;; Useful to define keys
(use-package general)
;; group commands under keybinding prefixes
(use-package hydra)

;;(global-set-key (kbd "C-c t") 'treemacs)

(general-define-key
 "C-x C-x" 'eval-last-sexp       ; evaluate expresion. put cursor after the exp and press kb
 "C-M-j" 'counsel-switch-buffer
 "C-c t" 'treemacs)

(defhydra hydra-text-scale (global-map "C-z")
  "scale text"                   ;;name 
  ("j" text-scale-increase "in")
  ("k" text-scale-decrease "out")
  ("f" nil "finished" :exit t))

(use-package counsel
  :bind (("M-x" . counsel-M-x)
         ("C-x b" . counsel-switch-buffer)
         ("C-x C-f" . counsel-find-file)
         :map minibuffer-local-map
         ("C-r" . 'counsel-minibuffer-history)))

;; completion engine
(use-package ivy
  ;; diminish avoids showing mode on screen
  :diminish
  ;; bind keys to things
  :bind (("C-s" . swiper)
	 ;; set key bindings for specific modes
         :map ivy-minibuffer-map
         ("TAB" . ivy-alt-done)	
         ("C-l" . ivy-alt-done)
         ("C-j" . ivy-next-line)
         ("C-k" . ivy-previous-line)
         :map ivy-switch-buffer-map
         ("C-k" . ivy-previous-line)
         ("C-l" . ivy-done)
         ("C-d" . ivy-switch-buffer-kill)
         :map ivy-reverse-i-search-map
         ("C-k" . ivy-previous-line)
         ("C-d" . ivy-reverse-i-search-kill))
  :config
  (ivy-mode 1))

;; gives info about the available keybindings while I type
;; I type Cx, and after 1 sec, it explains all the commands available after Cx
(use-package which-key
  :init (which-key-mode)
  :diminish which-key-mode
  :config (setq which-key-idle-delay 1))

;; include docs next to commands
(use-package ivy-rich
  :init
  (ivy-rich-mode 1))

;; better help functions
(use-package helpful
  :custom
  ;; when I say counsel.. call helpful...
  (counsel-describe-function-function #'helpful-callable)
  (counsel-describe-variable-function #'helpful-variable)
  :bind
  ([remap describe-function] . counsel-describe-function)
  ([remap describe-command] . helpful-command)
  ([remap describe-variable] . counsel-describe-variable)
  ([remap describe-key] . helpful-key))

;; domain-specific plugins
(use-package poly-markdown
  :ensure t)
(add-to-list 'auto-mode-alist '("\\.md" . poly-markdown-mode))

(use-package markdown-mode
  :ensure t
  :mode ("README\\..md\\'" . gfm-mode)
  :init (setq markdown-command "markdown"))

(setq markdown-enable-math t)
(setq markdown-enable-highlighting-syntax t)

(defun org-mode-setup ()
  (org-indent-mode)
  (org-toggle-pretty-entities)
  (variable-pitch-mode 1)
  (auto-fill-mode 0)
  (visual-line-mode 1)
  (set-face-attribute 'org-block nil :foreground nil :inherit 'fixed-pitch)
  (set-face-attribute 'org-table nil  :inherit 'fixed-pitch)
  (set-face-attribute 'org-formula nil  :inherit 'fixed-pitch)
  (set-face-attribute 'org-code nil   :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-indent nil :inherit '(org-hide fixed-pitch))
  (set-face-attribute 'org-verbatim nil :inherit '(shadow fixed-pitch))
  (set-face-attribute 'org-special-keyword nil :inherit '(font-lock-comment-face fixed-pitch))
  (set-face-attribute 'org-meta-line nil :inherit '(font-lock-comment-face fixed-pitch))
  (set-face-attribute 'org-checkbox nil :inherit 'fixed-pitch)
  ;; Get rid of the background on column views
  (set-face-attribute 'org-column nil :background nil)
  (set-face-attribute 'org-column-title nil :background nil))

(use-package org
  :hook (org-mode . org-mode-setup)
  :config (setq org-ellipsis " "
                org-hide-emphasis-markers t)) ;; hide * from bold
;; allow code blocks to be executed in org files
 (org-babel-do-load-languages
   'org-babel-load-languages
   '((emacs-lisp . t)
     (python . t)))
  (setq org-confirm-babel-evaluate nil)
(use-package org-superstar
  :after org
  :hook (org-mode . org-superstar-mode)
  :custom
  (org-superstar-headline-bullets-list '("" "" "" "" "" "" ""))
  (org-superstar-remove-leading-stars t))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(org-level-1 ((t (:inherit outline-1 :height 1.5))))
 '(org-level-2 ((t (:inherit outline-2 :height 1.3))))
 '(org-level-3 ((t (:inherit outline-3 :height 1.2))))
 '(org-level-4 ((t (:inherit outline-4 :height 1.0))))
 '(org-level-5 ((t (:inherit outline-5 :height 1.0)))))
;; Ensure that anything that should be fixed-pitch in Org files appears that way


(defun org-mode-visual-fill ()
  (setq visual-fill-column-width 170
        visual-fill-column-center-text t)
  (visual-fill-column-mode 1))

(use-package visual-fill-column
  :hook (org-mode . org-mode-visual-fill))

;; do not indent code blocks
(setq org-src-preserve-indentation t)

(require 'org-tempo)
(add-to-list 'org-structure-template-alist '("sh" . "src sh"))
(add-to-list 'org-structure-template-alist '("py" . "src python"))
(add-to-list 'org-structure-template-alist '("html" . "src html"))
(add-to-list 'org-structure-template-alist '("json" . "src json"))
(add-to-list 'org-structure-template-alist '("svelte" . "src svelte"))
(add-to-list 'org-structure-template-alist '("el" . "src emacs-lisp"))
(add-to-list 'org-structure-template-alist '("yaml" . "src yaml"))

(defun babel-config ()
  (let ((org-configm-babel-evaluate nil))
    (org-babel-tangle)))
(add-hook 'org-mode-hook (lambda () (add-hook 'after-save-hook #'babel-config)))

(use-package org-roam
  :ensure t
  :custom
  (org-roam-directory "~/roam-notes")
  (org-roam-completion-everywhere t)
  :bind (("C-c n l" . org-roam-buffer-toggle)
         ("C-c n f" . org-roam-node-find)
         ("C-c n i" . org-roam-node-insert)
         :map org-mode-map
         ("C-M-i" . completion-at-point))
  :config
  (org-roam-setup))

(setq split-height-threshold 100
      split-width-threshold 160)

;; (defun my-split-window-sensibly (&optional window)
;;     "replacement `split-window-sensibly' function which prefers vertical splits"
;;     (interactive)
;;     (let ((window (or window (selected-window))))
;;         (or (and (window-splittable-p window t)
;;                  (with-selected-window window
;;                      (split-window-right)))
;;             (and (window-splittable-p window)
;;                  (with-selected-window window
;;                      (split-window-below))))))

;; (setq split-window-preferred-function #'my-split-window-sensibly)

(use-package smartparens
  :config
  (smartparens-global-mode 1))

(defun indent-between-pair (&rest _ignored)
  (newline)
  (indent-according-to-mode)
  (forward-line -1)
  (indent-according-to-mode))

(sp-local-pair 'prog-mode "{" nil :post-handlers '((indent-between-pair "RET")))
(sp-local-pair 'prog-mode "[" nil :post-handlers '((indent-between-pair "RET")))
(sp-local-pair 'prog-mode "(" nil :post-handlers '((indent-between-pair "RET")))

(use-package yasnippet)
(yas-global-mode)

(define-key prog-mode-map (kbd "C-c >") (kbd "C-u 4 C-x TAB"))
(define-key prog-mode-map (kbd "C-c <") (kbd "C-u -4 C-x TAB"))

(use-package evil-nerd-commenter
  :bind ("M-/" . evilnc-comment-or-uncomment-lines))

;; (defun lsp-mode-setup ()
;;   (setq lsp-headerline-breadcrumb-segments '(path-up-to-project file symbols))
;;   (lsp-header-breadcrumb-mode))

(use-package lsp-mode
  :commands (lsp lsp-deferred)
  ;; :hook
  ;; (lsp-mode . lsp-mode-setup)
  :init
  (setq lsp-keymap-prefix "C-c l")
  (setq lsp-enable-snippet nil)
  :config
  (lsp-enable-which-key-integration t))
(setq lsp-ui-sideline-enable nil)
(add-hook 'lsp-mode-hook 'lsp-ui-mode)

(use-package lsp-ivy :commands lsp-ivy-workspace-symbol)
(use-package lsp-treemacs
  :after lsp)

(use-package company
  :after lsp-mode
  :hook (lsp-mode . company-mode)
  :bind (:map company-active-map
              ("<tab>" . company-complete-selection))
        (:map lsp-mode-map
              ("<tab>" . company-indent-or-complete-common))
  :custom
  (company-minimum-refix-length 2)
  (company-idle-delay 0.0))

(use-package company-box
  :hook (company-mode . company-box-mode))

(use-package lsp-ui
  :hook (lsp-mode . lsp-ui-mode)
  :custom
  (lsp-ui-doc-position 'bottom))

(use-package svelte-mode
  :mode "\\.svelte\\'"
  :hook (svelte-mode . lsp-deferred))

(use-package js2-mode
  :mode "\\.js\\'"
  :hook (js2-mode . lsp-deferred))
(use-package typescript-mode
  :mode "\\.ts\\'"
  :hook (typescript-mode . lsp-deferred)
  :config (setq typescript-indent-level 2))
(use-package json-mode)

(use-package emmet-mode
:after(web-mode css-mode scss-mode)
:commands (emmet-mode emmet-expand-line yas/insert-snippet yas-insert-snippet company-complete)
:config
(setq emmet-move-cursor-between-quotes t)
(add-hook 'emmet-mode-hook (lambda () (setq emmet-indent-after-insert nil)))
(add-hook 'sgml-mode-hook 'emmet-mode) ;; Auto-start on any markup modes
(add-hook 'css-mode-hook  'emmet-mode) ;; enable Emmet's css abbreviation.
;(setq emmet-indentation 2)
(unbind-key "C-M-<left>" emmet-mode-keymap)
(unbind-key "C-M-<right>" emmet-mode-keymap)
:bind
("C-j" . emmet-expand-line)
((:map emmet-mode-keymap
         ("C-c [" . emmet-prev-edit-point)
         ("C-c ]" . emmet-next-edit-point)))
);end emmet mode

(use-package python-mode
  :ensure nil
  :mode "\\.py\\'"
  :hook (python-mode . lsp-deferred)
  :custom
  (dap-python-debugger 'debugpy)
  (dap-python-executable "python")
  (python-shell-interpreter "python")
  :config
  (require 'dap-python))

(setq exec-path (append exec-path '("~/miniconda3/bin")))
(use-package lsp-pyright
  :ensure t
  :hook (python-mode . (lambda ()
                          (require 'lsp-pyright)
                          (lsp-deferred))))
(setq lsp-pyright-use-library-code-for-types t) ;; set this to nil if getting too many false positive type errors
(setq lsp-pyright-stub-path (concat (getenv "HOME") "/src/python-type-stubs"))
(setq lsp-pyright-venv-path (concat (getenv "HOME") "/miniconda3/envs"))

(use-package conda)
(require 'conda)
(custom-set-variables
 '(conda-anaconda-home "~/miniconda3"))
;; if you want interactive shell support, include:
(conda-env-initialize-interactive-shells)
;; if you want eshell support, include:
(conda-env-initialize-eshell)
;; if you want auto-activation (see below for details), include:
(conda-env-autoactivate-mode t)

(setq-default indent-tabs-mode nil)
(use-package wdl-mode)

(setq wdl-indent-level 4)
(use-package poly-wdl)

;; this seems redundant. TODO: look into how to change this
(require 'lsp-mode)
(add-to-list 'lsp-language-id-configuration '(wdl-mode . "wdl"))
(defgroup lsp-wdl nil
  "LSP support for WDL."
  :group 'lsp-mode
  :link '(url-link "https://github.com/broadinstitute/wdl-ide"))
(defcustom lsp-wdl-server-command "wdl-lsp"
  "Command to start wdl-lsp."
  :group 'lsp-wdl
  :risky t
  :type 'file)

(lsp-register-client
 (make-lsp-client :new-connection (lsp-stdio-connection
                                   (lambda () lsp-wdl-server-command))
                  :major-modes '(wdl-mode)
                  :priority -1
                  :server-id 'wdl))
(add-hook 'wdl-mode-hook #'lsp)

(use-package docker
  :ensure t
  :bind ("C-c d" . docker))
(use-package dockerfile-mode
  :mode ("Dockerfile\\..\\'" . dockerfile-mode))

(use-package projectile
  :diminish projectile-mode
  :config (projectile-mode)
  :custom (projectile-completion-system 'ivy)
  :bind-keymap
  ("C-c p" . projectile-command-map)
  :init
  (when (file-directory-p "~/Documents/gits")
    (setq projectile-project-search-path '("~/Documents/gits" "~/Documents/nus/projects")))
  (setq projectile-switch-project-action #'projectile-dired))
;; in Cp p, do M-o to get actions from counsel/ivy
(use-package counsel-projectile
  :config (counsel-projectile-mode))

(use-package magit
  :commands (magit-status magit-get-current-branch)
  :custom
  (magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1))

(use-package term
  :config
  (setq explicit-shell-file-name "bash")
  (setq term-prompt-regexp "^❯ *")) ; does not work
(use-package eterm-256color
  :hook (term-mode . eterm-256color-mode))

(use-package vterm
  :commands vterm
  :config
  (setq vterm-shell "bash")
  (setq vterm-max-scrollback 10000)
  (setq vterm-buffer-name-string "vterm %s"))

;; in the future, add shortcuts to launch terminals in different splits
(defhydra term-launch (global-map "C-x RET")
  ("RET" vterm "New terminal"))
